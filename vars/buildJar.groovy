#!/usr/bin/env groovy

def call() {
    echo "building the application for branch $BUILD_URL"
    sh 'mvn package'
}
